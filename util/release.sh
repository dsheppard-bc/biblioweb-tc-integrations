#!/bin/bash

CURRENT_DEV_VERSION=$(npm run getVersion -s)

git checkout -b release/$CURRENT_DEV_VERSION
git commit -m "Cut release/$CURRENT_DEV_VERSION branch"
git push origin release/$CURRENT_DEV_VERSION

git checkout develop
NEXT_DEV_VERSION=$(npm --no-git-tag-version version minor | cut -c 2-)
echo "Release version is $NEXT_DEV_VERSION"
git add package.json
git commit -m "Update develop version number to $NEXT_DEV_VERSION"
git push origin develop
