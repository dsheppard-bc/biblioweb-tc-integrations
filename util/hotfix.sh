#!/bin/bash

NEW_VERSION=$(npm --no-git-tag-version version patch | cut -c 2-)
echo "Hotfix version is $NEW_VERSION"

git checkout -b hotfix/$NEW_VERSION
git add package.json package-lock.json
git commit -m "Cut hotfix"
git push origin hotfix/$NEW_VERSION
